<?php

namespace Tests\Unit;

use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class CreatePaymentTest extends TestCase
{
    use WithFaker;
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function test_example()
    {
        // $this->assertTrue(true);
        $value = [
            'payment_name' => $this->faker->bankAccountNumber
        ];
        $this->post(route('create_payment'), $value)->assertStatus(200);
    }
}
