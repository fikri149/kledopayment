<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('payment', [App\Http\Controllers\Api\PaymentController::class, 'index'])->name('all_payment');
Route::post('payment', [App\Http\Controllers\Api\PaymentController::class, 'store'])->name('create_payment');
Route::delete('payment', [App\Http\Controllers\Api\PaymentController::class, 'destroy'])->name('delete_payment');
